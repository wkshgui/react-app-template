import React, { memo } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { Link } from "react-router-dom";

import { CloseOutlined } from '@ant-design/icons';

export class Tags extends React.Component{
  constructor(props) {
    super(props);
    this.state = {};
  }

  shouldComponentUpdate() {
    this.forceUpdate();
    return true;
  }

  render() {
    const { tagList, location } = this.props;
    return (
      <div className="flex">
        {tagList.map((item, index) => (
          <div
            key={`key_${index}`}
            className={item.value === location.pathname ? 'tag active': 'tag'}
          >
            <Link to={item.value} className="txt">{item.label}</Link>
            <CloseOutlined style={{marginLeft: '4px'}} onClick={() => this.props.deleteTag(item)} />
          </div>
        ))}
      </div>
    );
  }
};

Tags.propTypes = {
  tag: PropTypes.object,
  tagList: PropTypes.array.isRequired,
  deleteTag: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({});

const withConnect = connect(
  mapStateToProps,
);

export default compose(
  withConnect,
  memo,
)(Tags);
