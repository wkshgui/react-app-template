import React, { memo } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import website from 'cdn/website';
import { validatenull } from 'utils/validate';
import { Link } from "react-router-dom";
import { Menu } from 'antd';
import {
  UserOutlined,
  NodeCollapseOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;

export class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const menus = this.props.menu || []
    return (
      !menus.length ? (
        <div className="sidebar-tip">没有发现菜单</div>
      ) : (
        <Menu
          theme="dark"
          mode="inline"
          openKeys={this.props.openKeys}
          selectedKeys={this.props.selectedKeys}
          onOpenChange={keys => this.props.updateOpenKeys(keys)}
          onClick={({key}) => this.props.updateSelectedKeys([key])}
          style={{padding: '15px 0'}}
        >
          {menus.map(item => (
            validatenull(item[website.menu.props.children]) ? (
              <Menu.Item
                key={item.id}
                icon={<UserOutlined />}
                style={{ display: 'flex', alignItems: 'center' }}
              >
                <Link to={item[website.menu.props.path]}>{item[website.menu.props.name]}</Link>
              </Menu.Item>
            ) : (
              <SubMenu key={item.id} icon={<NodeCollapseOutlined />} title={item[website.menu.props.name]}>
                {item[website.menu.props.children].map(child => (
                  <Menu.Item key={child.id}>
                    <Link to={child[website.menu.props.path]}>{child[website.menu.props.name]}</Link>
                  </Menu.Item>
                ))}
              </SubMenu>
            )
          ))}
        </Menu>
      )
    );
  }
}

SideMenu.propTypes = {
  menu: PropTypes.array,
  openKeys: PropTypes.array,
  selectedKeys: PropTypes.array,
  updateOpenKeys: PropTypes.func,
  updateSelectedKeys: PropTypes.func,
}

const withConnect = connect();

export default compose(
  withConnect,
  memo
)(SideMenu);
