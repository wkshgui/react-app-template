import React, { memo } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getStore } from 'utils/storage';

function ProvideWithRoute(route) {
  const token = getStore({ name: 'access_token' });
  const { path, children, isAuth } = route;
  return (
    <Route
      path={path}
      render={props => (
        !token ? (
          path === '/login' ? (
            <Redirect to={{ pathname: '/', state: { from: props.location }}} />
          ) : (
            <route.component {...props} routes={children} />
          )
        ) : (
          isAuth ? (
            <Redirect to={{ pathname: '/login', state: { from: props.location }}} />
          ) : (
            <route.component {...props} routes={children} />
          )
        )
      )}
    />
  );
}

ProvideWithRoute.propTypes = {
  route: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({});

export function mapDispatchToProps(dispatch) {
  return {};
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo
)(ProvideWithRoute);
