import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConfigProvider } from 'antd';
import history from 'utils/history';
import { ConnectedRouter } from 'connected-react-router';
import configureStore from 'configureStore';
import zhCN from 'antd/lib/locale/zh_CN';
import 'antd/dist/antd.css';

import './index.css';
import App from 'containers/App';
import reportWebVitals from 'reportWebVitals';

const initialState = {};
const store = configureStore(initialState, history);
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ConfigProvider locale={zhCN}>
        <App />
      </ConfigProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
