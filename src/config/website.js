const website = {
  title: 'demo',
  copyright: 'Copyright © 2020 demo.com',
  key: 'demo',
  whiteList: ['/login', '/404', '/401', '/lock'],
  whiteTagList: ['/login', '/404', '/401', '/lock'],
  // 配置菜单的属性
  menu: {
    props: {
      label: 'label',
      name: 'name',
      path: 'path',
      icon: 'icon',
      children: 'children'
    }
  }
};

export default website;
