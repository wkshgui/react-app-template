import { createBrowserHistory } from 'history';
const history = createBrowserHistory();
// const history = createHashHistory();
export default history;
