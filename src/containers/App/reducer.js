/**
 * @author wk
 * @version 1.0.0
 * @description App reducer
 * @date 2021/07/02
 */

 import produce from 'immer';

 // The initial state of the App
 export const initialState = {
   loading: false,
   error: false,
   currentUser: false
 };
 
 /* eslint-disable default-case, no-param-reassign */
 const appReducer = (state = initialState, action) =>
   produce(state, draft => {
     switch (action.type) {}
   });
 
 export default appReducer;
  