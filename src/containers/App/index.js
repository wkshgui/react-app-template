import React from "react";
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';
import GlobalStyle from 'global-styles';

import routes from 'router';
import ProvideWithRoute from 'components/ProvideWithRoute';
import NotFoundPage from 'components/NotFoundPage';

function App() {
  const AppWrapper = styled.div`
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
    font-family: Chinese Quote, -apple-system, BlinkMacSystemFont, Segoe UI,
      PingFang SC, Hiragino Sans GB, Microsoft YaHei, Helvetica Neue, Helvetica,
      Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;
    font-size: 14px;
  `;

  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s | react-app模版"
        defaultTitle="react-app模版"
      >
        <meta name="description" content="react-app模版" />
      </Helmet>
      <Switch>
        { routes.map((route, i) => (
          <ProvideWithRoute key={i} {...route} />
        )) }
        <Route component={ NotFoundPage } />
      </Switch>
      <GlobalStyle />
    </AppWrapper>
  );
}

export default App;
